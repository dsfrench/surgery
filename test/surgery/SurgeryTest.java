/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package surgery;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Dan
 */
public class SurgeryTest {
    
    /**
     * Test of find person method, of class Person.
     */
    @Test
    public void testPerson() {
        Contact contact1 = new Contact("4", "The Mews", "Penzance", "TR182SA", "01736358477");
        Person person1 = new Person("Dan", "04/03/1988", "m", contact1);
        Contact contact2 = new Contact("4", "The Mews", "Penzance", "TR182SA", "01736358477");
        Person person2 = new Person("Bob", "04/03/1983", "m", contact2);
        assertEquals("Dan", scene.getObject(4, 2));      
    }
    
}
