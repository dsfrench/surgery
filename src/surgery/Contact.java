/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package surgery;

/**
 *
 * @author Dan
 */
public class Contact {
    private String house;
    private String road;
    private String town;
    private String postcode;
    private String phoneNum;
    
    public Contact (String h, String r, String t, String p, String ph){
        this.house = h;
        this.road = r;
        this.town = t;
        this.postcode = p;
        this.phoneNum = ph;
    }
}
